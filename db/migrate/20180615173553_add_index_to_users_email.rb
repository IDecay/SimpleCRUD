class AddIndexToUsersEmail < ActiveRecord::Migration[5.2]
  def change
    add_index :users, 'lower(email)', unique: true
  end
end
