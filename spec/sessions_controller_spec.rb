require 'rails_helper'
require 'rspec-rails'
require 'spec_helper'

describe SessionsController, :type => :controller do
  context 'When user logins with "Remember me"' do
    it 'should have a cookie for user' do
      @user = User.new name: 'StrongErection', email: 'glazina1995@gmail.com', password_digest: '$2a$10$Y2OMq2EtmpC0qojwRoXiNO/KUgAMpnjYYzJ22U7lEbnvQQ9dGJtAq'
      @user.save
      post 'create', params: { session: { email: @user.email,
                                              password: @user.password,
                                              remember_me: 1 } }
      expect(response.cookies['remember_token']).to_not be_nil
    end
  end
  context 'When user logins without "Remember me"' do
    it 'should not have a cookie for user' do
      @user = User.new name: 'StrongErection', email: 'glazina1995@gmail.com', password_digest: '$2a$10$Y2OMq2EtmpC0qojwRoXiNO/KUgAMpnjYYzJ22U7lEbnvQQ9dGJtAq'
      @user.save
      post 'create', params: { session: { email: @user.email,
                                                   password: 'lineage',
                                                   remember_me: 1 } }

      expect(response.cookies['remember_token']).to be_nil
    end
  end
end