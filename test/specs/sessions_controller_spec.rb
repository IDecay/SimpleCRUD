require 'rspec'
require 'test_helper'

describe 'Login behaviour' do

  login_helper = LoginHelper.new
  context 'When user logins with "Remember me"' do
    it 'should have a cookie for user' do
      @user = User.find_by_email('glazina1995@gmail.com')
      login_helper.log_in_as(@user, password: 'lineage', remember_me: '1')
      expect(cookies['remember_token']).to_not be_nil
    end
  end
  context 'When user logins without "Remember me"' do
    it 'should not have a cookie for user' do
      @user = User.find_by_email('glazina1995@gmail.com')
      login_helper.log_in_as(@user, password: 'lineage', remember_me: '1')
      login_helper.log_in_as(@user, password: 'lineage', remember_me: '0')
      expect(cookies['remember_token']).to be_nil
    end
  end
end